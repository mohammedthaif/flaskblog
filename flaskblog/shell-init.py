from flaskblog import app, db
from sqlalchemy import func

from flaskblog.models import User, Like, Post

users = User.query.all()
posts = Post.query.all()
likes = Like.query.all()

